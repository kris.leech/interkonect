+++
date = "2017-04-18T10:17:20+01:00"
title = "Work experience students visit from Djanogly"
weight = 100
image = 'students.jpg'
orient = "left"
+++

We have had two students over on work experience from the [Djanogly Academy in Nottingham](http://www.djanogly.notts.sch.uk/) which specialises in Information and Communication Technology (ICT).

<!--more-->

We let the two students get hands-ons during the various stages of designing & deploying a website. They had a direct influence on the final layout and logo.

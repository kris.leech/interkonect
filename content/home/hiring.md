+++
date = "2017-04-18T10:17:20+01:00"
title = "We are hiring"
image = "we-are-hiring.jpg"
orient = "right"
+++


We are looking for the right person to join our development team.

Your personal qualities are more important than the tools and language you have experience with.

Training in RubyOnRails can be provided.
Some opportunity to work from home.

Please get in touch.




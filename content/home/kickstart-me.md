+++
date = "2017-04-18T10:17:20+01:00"
title = "Kickstart Me"
orient = "left"
weight = 0
image = "kickstart-me.jpg"
+++

Bespoke Ruby/Ruby on Rails coaching for your team.

We will be providing technical training for website developers across the East Midlands.

<!--more-->

Enrolments are now being taken for our first RubyOnRails Training event at the [EMCC](https://www.nottingham.ac.uk/conference/facilities/emcc/index.aspx).

In the future we also aim to hold workshops and training on XHTML, CSS, and JQuery (the write more do less javascript library).



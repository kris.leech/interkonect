+++
date = "2017-04-18T10:17:20+01:00"
title = "Interkonect contributes to Digital Media Design Degree Curriculum"
orient = "right"
image = 'digital-media.jpg'
+++

During 2006 Interkonect contributed to the design of the Digital Media Design top-up degree at [New College Nottingham](https://www.ncn.ac.uk/), which is validated by Nottingham Trent University.

<!--more-->

Kris Leech offered advice and guidance in the development of the curriculum based on forecast trends in the industry and desirable graduate profiles.

In 2007, Interkonect tendered a real-time brief to this new degree programme, for each student on the course gets to work on a brief set by a representative of the creative sector. As part of Interkonect's ongoing commitment to education in the region, a third year multimedia student was asked to redevelop the Interkonect website during this live project.

The student in question is now employed within the private sector. Based on his achievements during the programme, and on a strong final show, he is now a commercial web designer.

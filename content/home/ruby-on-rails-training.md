+++
date = "2017-04-18T10:17:20+01:00"
title = "Ruby & Rails Training in London"
image = "ruby-on-rails-training.jpg"
weight = -50
style = "5"
orient = "right"
+++

Interkonect has teamed up with [Impartica IT Training](www.impartica-training.co.uk) to provide expert training for both Ruby and Rails.

The courses run on consecutive days and are complimentary.

# interkonect.com static website

Hosted on Gitlab: https://gitlab.com/kris.leech/interkonect

Uses:

* [Hugo](https://gohugo.io/)
* [Story](https://html5up.net/story)

## Deployment

Automatic on git push using [Render](https://render.com)

## Development

```
hugo server --watch
```

Then visit `http://localhost:1313`.

To compress the static images run:

```
bin/compress-images
```

## Build

```
hugo
```
